import numpy as np
import matplotlib.pyplot as plt

N_values = [10, 20, 30, 40, 50]
M_values = [10, 20, 30, 40, 50]
maxT = float(input("What is the max value of alpha? (=D_e*t/epsilon_w*Lf^2)"))
phi = float(input("What does the thiele modulus phi equal?"))
a = phi**2
L = float(input("How long is the system?"))
H = float(input("How tall is the system?"))

for count in range(0, 5):
    # SETUP
    N = N_values[count]   # number of x axis components
    M = M_values[count]    # number of y axis components

    T = int(np.ceil(2*maxT * ((N**2)*(H/L) + (M**2)*(L/H))))
    x = np.linspace(0., 1., N+1)
    y = np.linspace(0., 1., M+1)
    t = np.linspace(0., maxT, T+1)

    dX = x[1] -x[0]
    dY = y[1] -y[0]
    dT = t[1] -t[0]

    rX = (dT/(dX**2)) * (H/L)
    rY = (dT/(dY**2)) * (L/H)

    print("rX = {0}".format(rX))
    print("rY = {0}".format(rY))

    U = np.zeros((N+1,M+1))
    U_new = np.zeros((N+1,M+1))

    Mag = np.zeros(T+1)

    # INITIAL CONDITIONS
    for j in range(0, M+1):
        U[-1,j] = U_new[-1,j] = 1  # outer area condition

    # LOOP

    for s in range(0, T):
        for j in range(1, M):
            for i in range(1, N):
                U_new[i, j] = (1-a*dT)*U[i, j] + \
                              rX * (U[i + 1, j] - 2*U[i, j] + U[i - 1, j]) +\
                              rY * (U[i, j + 1] - 2*U[i, j] + U[i, j - 1])
            U_new[1, j + 1] = U_new[0, j] = U_new[1, j]

        Mag[s] = np.linalg.norm(U)
        U = U_new
        if (s % 100) == 0:
            print("[{0}] {1} / {2}".format(count, s, T))
    Mag = Mag/Mag[-5]
    plt.plot(t, Mag, label="N={0}, M={1}, rX={2:.2f}, rY={3:.2f}".format(N, T, rX, rY))

plt.title('Diffusion of a irriversibly sorbing nonreacting solute ("2D HOMOGENEOUS)')
x1, x2, y1, y2 = plt.axis()
plt.xlabel('$\\alpha$', fontsize=12)
plt.ylabel('$|U/U_{\\max}|$', fontsize=12)
plt.legend(loc='upper right')
# plt.savefig("plot2.pdf", bbox_inches='tight')
plt.show()