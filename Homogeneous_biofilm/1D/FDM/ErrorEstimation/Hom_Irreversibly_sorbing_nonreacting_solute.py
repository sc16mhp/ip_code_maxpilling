import numpy as np
import matplotlib.pyplot as plt

# This code allows a user to, once creating a function to describe the
# diffusivity of a irreversible sorbing nonreacting solute,
# and putting it in terms of non dimensional variables, AND finding
# the spatial derivative of said function. They can input the
# function into d(x,t) and d_dash(x,t) (derivative) and then they can
# run the code to generate results. The user inputs the MAX value
# of t at run time.
# Setting this too high, can render the project unstable.

N_values = [10, 20, 30, 40, 50]
maxT = float(input("What is the max value of alpha? (=D_e*t/epsilon_w*Lf^2)"))
maxW = 1
phi = float(input("What does the thiele modulus phi equal?"))
rho = float(input("What does rho equal?"))
a = phi**2
b = phi**2/rho
count = 0
for N in N_values:

    # SETUP
    T = int(np.ceil(maxT*(1)*(a*maxW/2 + 2*N**2)))  # number of grid points in t axis
    x = np.linspace(0., 1., N+1)
    t = np.linspace(0., maxT, T+1)

    dX = x[1] - x[0]
    dT = t[1] - t[0]

    r = dT/(dX**2)

    print("r = {0}".format(r))
    # input("Press enter to continue.")

    U = np.zeros(N+1)
    U_new = np.zeros(N+1)
    W = np.zeros(N+1)
    W_new = np.zeros(N+1)

    Error = np.zeros(T+1)

    # INITIAL CONDITIONS
    for i in range(0, N+ 1):
        U[i] = 0
        W[i] = 1

    # LOOP THROUGH TIME STEPS
    for j in range(0, T):
        for i in range(1, N):
            U_new[i] = r*U[i+1] + (1-2*r - a*dT*W[i])*U[i] + r*U[i-1]
            W_new[i] = (1-b*dT*U[i])*W[i]

        U_new[0] = U_new[1]     # no flux constraint
        U_new[-1] = 1
        W[:] = W_new

        Error[j] = np.linalg.norm((U_new - U))
        U[:] = U_new
        if (j % 100) == 0:
            print("[{0}] {1} / {2}".format(count, j, T))
    plt.plot(t, np.log(Error), label="N={0}, M={1}, r={2:.2f}".format(N, T, r))
    count = count + 1

#plt.title('Diffusion of an irreversibly sorbing, nonreacting solute (HOMOGENEOUS)')
x1, x2, y1, y2 = plt.axis()
plt.xlabel('$\\alpha$', fontsize=12)
plt.ylabel('log(Error)', fontsize=12)
plt.legend(loc='upper right')
plt.savefig('ERROR_Hom_1D_IrreversiblyAAA.pdf')
plt.show()