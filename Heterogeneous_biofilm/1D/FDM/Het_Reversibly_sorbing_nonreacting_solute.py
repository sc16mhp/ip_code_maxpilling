import numpy as np
import matplotlib.pyplot as plt

# THIS CODE IS IDENTICAL TO Nonreacting_solute. THE ONLY DIFFERENCE
# IS THAT ALPHA IS SOME CONSTANT*ALPHA FROM THE PREVIOUS CODE
# NON DIMENSIONALISE AND USE EITHER.

# This code allows a user to, once creating a function to describe the
# diffusivity of a non-interacting biofilm, and putting it in terms of
# non dimensional variables, AND finding the spatial derivative of said
# function. They can input the function into d(x,t) and
# d_dash(x,t) (derivative) and then they can run the code to generate
# results. The user inputs the MAX value at run time for their non
# dimensional variable that replaces time. Setting this too high, can
# render the project unstable.


def d(x,t):  # add your non dimensional function D(x,t) here
    Do = 0.02
    Dinf = 0.6
    return Do*(1 - x) + Dinf*x


def d_dash(x,t): # add the spatial derivative of your non dimensional function D(x,t) here
    Do = 0.02
    Dinf = 0.6
    return Dinf - Do


# SETUP
N = 100     # number of grid points in the x axis

maxT = float(input("What is the max value of alpha? (=t/epsilon_w*Lf^2)"))
derivative_known = bool(input("Is the derivative known? 1 or 0."))

T = int(np.ceil(2 * maxT * N**2))  # number of grid points in t axis

x = np.linspace(0., 1., N+1)
t = np.linspace(0., maxT, T+1)


dX = x[1] - x[0]
dT = t[1] - t[0]

r = (dT/dX**2)

print("r = {0}".format(r))
input("Press enter to continue.")

U = np.zeros(N+1)
U_new = np.zeros(N+1)

# INITIAL CONDITIONS
for i in range(0, N+1):
    U[i] = 0

# LOOP THROUGH TIME STEPS
for j in range(0, T):
    print("[0] {0}/{1}".format(j, T))
    for i in range(1, N):
        if derivative_known:
            U_new[i] = r*(dX*d_dash(i*dX, j*dT) + d(i*dX, j*dT))*U[i+1] +\
                (1 - r*(dX*d_dash(i*dX, j*dT) + 2*d(i*dX, j*dT)))*U[i] +\
                r*d(i*dX, j*dT)*U[i-1]
        else:
            U_new[i] = r*d((i+1)*dX, j*dT)*U[i+1] +\
                (1 - r*(d((i+1)*dX, j*dT) + d(i*dX, j*dT)))*U[i] +\
                r*d(i*dX, j*dT)*U[i-1]

    U_new[0] = U_new[1]     # no flux constraint
    U_new[-1] = 1
    U[:] = U_new

    if j == int(T / 512):
        plt.plot(x, U, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    elif j == int(T / 128):
        plt.plot(x, U, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    elif j == int(T / 32):
        percent = int(100 / 32)
        print("{0:}% done".format(percent))
        plt.plot(x, U, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    elif j == int(T / 16):
        percent = int(100 / 16)
        print("{0:}% done".format(percent))
        plt.plot(x, U, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    elif j == int(T / 8):
        percent = int(100 / 8)
        print("{0:}% done".format(percent))
        plt.plot(x, U, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    elif j == int(T / 4):
        percent = int(25)
        print("{0:}% done".format(percent))
        plt.plot(x, U, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    elif j == int(T / 2):
        percent = int(50)
        print("{0:}% done".format(percent))
        plt.plot(x, U, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    elif j == int(3 * T / 4):
        percent = 75
        print("{0:}% done".format(percent))
        plt.plot(x, U, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    elif j == T - 1:
        print("100% done")
        plt.plot(x, U, label="$\\alpha = ${0:.4f}".format(maxT))


plt.title('Diffusion of a reversibly sorbing, nonreacting solute')
plt.xlabel('$z$', fontsize=12)
plt.ylabel('$C$', fontsize=12)
plt.legend(loc='upper right')
# plt.savefig('NHeterogeneous.pdf')
plt.show()