import numpy as np
import matplotlib.pyplot as plt


def d(x,y,t):  # add your non dimensional function D(x,t) here
    return 0.8*x*(1-(1-2*y)**2) + 0.05*(1-x)


def d_dash_x(x,y,t): # add the spatial derivative of your non dimensional function D(x,t) here
    return 0.8*(1-(1-2*y)**2) - 0.05


def d_dash_y(x,y,t):
    return 4*0.8*x*(1-2*y)


# SETUP
N = 40   # number of x axis components
M = 40    # number of y axis components

maxT = float(input("What is the max value of alpha? (=t/epsilon_w*Lf^2)"))
derivative_known = bool(input("Is the derivative known? 1 or 0."))
L = float(input("How long is the system? (L)"))
H = float(input("How tall is the system? (H)"))


T = int(np.ceil(2*maxT * ((N**2)*(H/L) + (M**2)*(L/H))))
x = np.linspace(0., 1., N+1)
y = np.linspace(0., 1., M+1)
t = np.linspace(0., maxT, T+1)

dX = x[1] -x[0]
dY = y[1] -y[0]
dT = t[1] -t[0]

rX = (dT/(dX**2)) * (H/L)
rY = (dT/(dY**2)) * (L/H)

print("rX = {0}".format(rX))
print("rY = {0}".format(rY))
input("Press enter to continue.")

U = np.zeros((N+1, M+1))
U_new = np.zeros((N+1, M+1))

# INITIAL CONDITIONS
for j in range(0, M+1):
    U[-1, j] = U_new[-1, j] = 1  # outer area condition

# LOOP
for s in range(0, T):
    for j in range(1, M):
        for i in range(1, N):
            if derivative_known:
                U_new[i, j] = U[i, j] +\
                    rX*((d_dash_x(i*dX, j*dY, s*dT)*dX + d(i*dX, j*dY, s*dT))*U[i+1, j] -
                        (d_dash_x(i*dX, j*dY, s*dT)*dX + 2*d(i*dX, j*dY, s*dT))*U[i, j] +
                        d(i*dX, j*dY, s*dT)*U[i-1, j]) +\
                    rY*((d_dash_x(i*dX, j*dY, s*dT)*dY + d(i*dX, j*dY, s*dT))*U[i, j+1] -
                        (d_dash_x(i*dX, j*dY, s*dT)*dY + 2*d(i*dX, j*dY, s*dT))*U[i, j] +
                        d(i*dX, j*dY, s*dT)*U[i, j-1])
            else:
                U_new[i, j] = U[i, j] + \
                          rX * (d((i+1)*dX,j*dY,s*dT)*U[i + 1, j] -
                                (d((i+1)*dX,j*dY,s*dT)+d(i*dX,j*dY,s*dT))*U[i, j] +
                                d(i*dX,j*dY,s*dT)*U[i - 1, j]) +\
                          rY * (d(i*dX,(j+1)*dY,s*dT)*U[i, j + 1] -
                                (d(i*dX,(j+1)*dY,s*dT) + d(i*dX,j*dY,s*dT))*U[i, j] +
                                d(i*dX,j*dY,s*dT)*U[i, j - 1])
        U_new[1, j + 1] = U_new[0, j] = U_new[1, j]
    U = U_new
    if ((s % 50) == 0):
        print("[0] {0} / {1}".format(s, T))
        fig = plt.figure()
        str = "t: {0}/{1}".format(s,T)
        plt.plot([], [], ' ', label=str)
        plt.legend(loc='upper right', frameon=True)
        #plt.title("2D Diffusion of a noninteracting solute in a heterogeneous biofilm")
        plt.xlabel("$D=0.8x(1-(1-2y)^2) + 0.05(1-x)$")
        plt.imshow(U,vmin=0, vmax=1)
        plt.colorbar();
        plt.savefig("../Plots/Noninteracting/C/%d"%s, dpi=fig.dpi, bbox_inches='tight')
        plt.show()
        fig.clear()
        plt.close(fig)
    else:
        print("[0] {0}".format(s))