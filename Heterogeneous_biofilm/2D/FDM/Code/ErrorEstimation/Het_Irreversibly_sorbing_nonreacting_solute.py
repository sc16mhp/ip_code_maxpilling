import numpy as np
import matplotlib.pyplot as plt


def d(x,y,t):  # add your non dimensional function D(x,t) here
    return 0.6*x+0.1*(1-x)


def d_dash_x(x,y,t): # add the spatial derivative of your non dimensional function D(x,t) here
    return 0.6 - 0.1


def d_dash_y(x,y,t):
    return 0


N_values = [10, 20, 30, 40]
M_values = [10, 20, 30, 40]

maxT = float(input("What is the max value of alpha you want to got to?"))
maxW = 1
phi = float(input("What does the thiele modulus phi equal?"))
rho = float(input("What does rho equal?"))
a = phi**2
b = phi**2/rho
L = float(input("How long is the system?"))
H = float(input("How tall is the system?"))
derivative_known = bool(input("Is the derivative known? 1 or 0."))

for count in range(0, 4):
    # SETUP
    N = N_values[count]   # number of x axis components
    M = M_values[count]    # number of y axis components

    T = int(np.ceil(maxT * ((a*maxW)/2 + 2*(N**2)*(H/L) + 2*(M**2)*(L/H))))
    x = np.linspace(0., 1., N+1)
    y = np.linspace(0., 1., M+1)
    t = np.linspace(0., maxT, T+1)

    dX = x[1] -x[0]
    dY = y[1] -y[0]
    dT = t[1] -t[0]

    rX = (dT/(dX**2)) * (H/L)
    rY = (dT/(dY**2)) * (L/H)

    print("rX = {0}".format(rX))
    print("rY = {0}".format(rY))

    U = np.zeros((N+1,M+1))
    U_new = np.zeros((N+1,M+1))

    W = np.full((N+1,M+1), 1)
    W_new = np.zeros((N+1,M+1))

    Mag = np.zeros(T + 1)

    # INITIAL CONDITIONS
    for j in range(0, M+1):
        U[-1,j] = U_new[-1,j] = 1  # outer area condition

    # LOOP
    for s in range(0, T):
        for j in range(1, M):
            for i in range(1, N):
                if derivative_known:
                    U_new[i, j] = (1-a*dT*W[i,j])*U[i, j] + \
                                  rX * ((d_dash_x(i * dX, j * dY, s * dT) * dX + d(i * dX, j * dY, s * dT)) * U[i + 1, j] -
                                        (d_dash_x(i * dX, j * dY, s * dT) * dX + 2 * d(i * dX, j * dY, s * dT)) * U[i, j] +
                                        d(i * dX, j * dY, s * dT) * U[i - 1, j]) + \
                                  rY * ((d_dash_x(i * dX, j * dY, s * dT) * dY + d(i * dX, j * dY, s * dT)) * U[i, j + 1] -
                                        (d_dash_x(i * dX, j * dY, s * dT) * dY + 2 * d(i * dX, j * dY, s * dT)) * U[i, j] +
                                        d(i * dX, j * dY, s * dT) * U[i, j - 1])
                else:
                    U_new[i, j] = (1-a*dT*W[i,j])*U[i, j] + \
                                  rX * (d((i + 1) * dX, j * dY, s * dT) * U[i + 1, j] -
                                        (d((i + 1) * dX, j * dY, s * dT) + d(i * dX, j * dY, s * dT)) * U[i, j] +
                                        d(i * dX, j * dY, s * dT) * U[i - 1, j]) + \
                                  rY * (d(i * dX, (j + 1) * dY, s * dT) * U[i, j + 1] -
                                        (d(i * dX, (j + 1) * dY, s * dT) + d(i * dX, j * dY, s * dT)) * U[i, j] +
                                        d(i * dX, j * dY, s * dT) * U[i, j - 1])
                W_new[i, j] = (1 - b*dT*U[i,j])*W[i,j]
            U_new[1, j + 1] = U_new[0, j] = U_new[1, j]

        Mag[s] = np.linalg.norm(U)
        U = U_new
        W = W_new
        if (s % 100) == 0:
            print("[{0}] {1} / {2}".format(count, s, T))
    Mag = Mag / Mag[-5]
    plt.plot(t, Mag, label="N={0}, M={1}, rX={2:.2f}, rY={3:.2f}".format(N, T, rX, rY))

#plt.title('Diffusion of a non-interacting solute ("2D HETEROGENEOUS)')
x1, x2, y1, y2 = plt.axis()
plt.xlabel('$\\alpha$', fontsize=12)
plt.ylabel('$|U/U_{\\max}|$', fontsize=12)
plt.legend(loc='upper right')
plt.savefig("Err_Het_Irreversibly.pdf", bbox_inches='tight')
plt.show()
