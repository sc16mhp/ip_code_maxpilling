import numpy as np
import matplotlib.pyplot as plt

# This code allows a user to, once creating a function to describe the
# diffusivity of a non-interacting biofilm, and putting it in terms of
# non dimensional variables, AND finding the spatial derivative of said
# function. They can input the function into d(x,t) and
# d_dash(x,t) (derivative) and then they can run the code to generate
# results. The user inputs the MAX value at run time for their non
# dimensional variable that replaces time. Setting this too high, can
# render the project unstable.


def d(x,t):  # add your non dimensional function D(x,t) here
    Do = 0.02
    Dinf = 0.6
    return Do*(1 - x) + Dinf*x


def d_dash(x,t): # add the spatial derivative of your non dimensional function D(x,t) here
    Do = 0.02
    Dinf = 0.6
    return Dinf - Do


N_values = [10, 20, 30, 40, 50]
maxT = float(input("What is the max value of alpha? (=D_e*t/epsilon_w*Lf^2)"))

derivative_known = bool(input("Is the derivative known? 1 or 0."))
count = 0
for N in N_values:
    # SETUP
    T = int(np.ceil(2 * maxT * N**2))  # number of grid points in t axis

    x = np.linspace(0., 1., N+1)
    t = np.linspace(0., maxT, T+1)

    dX = x[1] - x[0]
    dT = t[1] - t[0]

    r = (dT/dX**2)

    print("r = {0}".format(r))
    input("Press enter to continue.")

    U = np.zeros(N+1)
    U_new = np.zeros(N+1)

    Error = np.zeros(T + 1)

    # INITIAL CONDITIONS
    for i in range(0, N+1):
        U[i] = 0

    # LOOP THROUGH TIME STEPS
    for j in range(0, T):
        for i in range(1, N):
            if derivative_known:
                U_new[i] = r*(dX*d_dash(i*dX, j*dT) + d(i*dX, j*dT))*U[i+1] +\
                    (1 - r*(dX*d_dash(i*dX, j*dT) + 2*d(i*dX, j*dT)))*U[i] +\
                    r*d(i*dX, j*dT)*U[i-1]
            else:
                U_new[i] = r*d((i+1)*dX, j*dT)*U[i+1] +\
                    (1 - r*(d((i+1)*dX, j*dT) + d(i*dX, j*dT)))*U[i] +\
                    r*d(i*dX, j*dT)*U[i-1]

        U_new[0] = U_new[1]     # no flux constraint
        U_new[-1] = 1
        Error[j] = np.linalg.norm((U_new - U))
        U[:] = U_new
        if (j % 100) == 0:
            print("[{0}] {1} / {2}".format(count, j, T))
    plt.plot(t, np.log(Error), label="N={0}, M={1}, r={2:.2f}".format(N, T, r))
    count = count + 1

#plt.title('Diffusion of a non-interacting solute (HETEROGENEOUS)')
# x1, x2, y1, y2 = plt.axis()
# plt.axis((-0.01 ,0.5*maxT, y1, y2))
plt.xlabel('$\\alpha$', fontsize=12)
plt.ylabel('log(Error)', fontsize=12)
plt.legend(loc='upper right')
plt.savefig('ERROR_Het_1D_NoninteractingA.pdf')
plt.show()