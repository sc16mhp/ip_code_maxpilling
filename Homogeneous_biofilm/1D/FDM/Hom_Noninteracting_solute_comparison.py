import numpy as np
import matplotlib.pyplot as plt

# This code compares a numerical solution to an analytical solution for eq (4.3)


def analytical(x,t):
    sum_total = 0
    const = 0
    e = 0
    c = 0
    for n in range(0,1000):
        const = (-1)**n / ((n+1/2)*np.pi)
        e = np.exp(-((n+1/2)*np.pi)**2*t)
        c = np.cos((n+1/2)*np.pi*x)
        sum_total = sum_total + const*e*c
    return 1 - 2*sum_total


# SETUP
N_values = [10,30,50,70,100]
maxT = float(input("What is the max value of alpha? (=D_e*t/epsilon_w*Lf^2)"))
for N in N_values:

    
    T = int(np.ceil(2 * maxT * N**2))  # number of grid points in t axis
    x = np.linspace(0., 1., N+1)
    t = np.linspace(0., maxT, T+1)
    
    dX = x[1] - x[0]
    dT = t[1] - t[0]
    
    r = (dT/dX**2)
    
    print("r = {0}".format(r))
    #input("Press enter to continue.")
    
    U = np.zeros(N+1)
    U_new = np.zeros(N+1)
    
    A = np.zeros(N+1)
    diff = np.zeros(T+1)
    
    # INITIAL CONDITIONS
    for i in range(0, N+1):
        U[i] = 0
    
    # LOOP THROUGH TIME STEPS
    for j in range(0, T):
        for i in range(1, N):
            U_new[i] = r*U[i+1] + (1 - 2*r)*U[i] + r*U[i-1]
        U_new[0] = U_new[1]     # no flux constraint
        U_new[-1] = 1           # Boundary condition
        U[:] = U_new
        print(j)
    # =============================================================================
    #     if j == int(T / 128):
    #         A = analytical(x,t[j])
    #         diff = A-U
    #         plt.plot(x, diff, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    #     elif j == int(T / 32):
    #         A = analytical(x,t[j])
    #         diff = A-U
    #         plt.plot(x, diff, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    #     elif j == int(T / 16):
    #         A = analytical(x,t[j])
    #         diff = A-U
    #         plt.plot(x, diff, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    #     elif j == int(T / 8):
    #         A = analytical(x,t[j])
    #         diff = A-U
    #         plt.plot(x, diff, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    #     elif j == int(T / 4):
    #         A = analytical(x,t[j])
    #         diff = A-U
    #         plt.plot(x, diff, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    #     elif j == int(T / 2):
    #         A = analytical(x,t[j])
    #         diff = A-U
    #         plt.plot(x, diff, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    #     elif j == int(3*T / 4):
    #         A = analytical(x,t[j])
    #         diff = A-U
    #         plt.plot(x, diff, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    #     elif j == int(T-1):
    #         A = analytical(x,t[j])
    #         diff = A-U
    #         plt.plot(x, diff, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    # =============================================================================
        A = analytical(x,t[j])
        diff[j] = np.linalg.norm(A-U)
    
    plt.plot(t, diff, label="N = {0}, r = {1}".format(N, r))
    
    
plt.xlabel('$\\alpha$', fontsize=12)
plt.ylabel('Difference', fontsize=12)
plt.legend(loc='upper right')
plt.savefig('Hom_1D_Noninteracting_Difff.pdf', bbox_inches='tight')
plt.show()