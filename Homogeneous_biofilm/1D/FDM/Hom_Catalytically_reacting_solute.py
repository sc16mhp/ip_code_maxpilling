import numpy as np
import matplotlib.pyplot as plt

# This code allows a user to, once creating a function to describe the
# diffusivity of a non-interacting biofilm, and putting it in terms of
# non dimensional variables, AND finding the spatial derivative of said
# function. They can input the function into d(x,t) and
# d_dash(x,t) (derivative) and then they can run the code to generate
# results. The user inputs the MAX value at run time for their non
# dimensional variable that replaces time. Setting this too high, can
# render the project unstable.

# SETUP
N = 100     # number of grid points in the x axis

maxT = float(input("What is the max value of alpha you want to got to?"))
phi = float(input("What does the thiele modulus phi equal?"))
a = phi**2

T = int(np.ceil(maxT*(a/2 + 2*N**2)))  # number of grid points in t axis
x = np.linspace(0., 1., N+1)
t = np.linspace(0., maxT, T+1)

dX = x[1] - x[0]
dT = t[1] - t[0]

r = dT/(dX**2)

print("r = {0}".format(r))
input("Press enter to continue.")

U = np.zeros(N+1)
U_new = np.zeros(N+1)

# INITIAL CONDITIONS
for i in range(0, N+ 1):
    U[i] = 0

# LOOP THROUGH TIME STEPS
for j in range(0, T):
    for i in range(1, N):
        U_new[i] = r*U[i+1] + (1-2*r - a*dT)*U[i] + r*U[i-1]

    U_new[0] = U_new[1]     # no flux constraint
    U_new[-1] = 1
    U[:] = U_new
    if j == int(T / 512):
        plt.plot(x, U, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    elif j == int(T / 128):
        plt.plot(x, U, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    elif j == int(T / 32):
        percent = int(100 / 32)
        print("{0:}% done".format(percent))
        plt.plot(x, U, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    elif j == int(T / 16):
        percent = int(100 / 16)
        print("{0:}% done".format(percent))
        plt.plot(x, U, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    elif j == int(T / 8):
        percent = int(100 / 8)
        print("{0:}% done".format(percent))
        plt.plot(x, U, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    elif j == int(T / 4):
        percent = int(25)
        print("{0:}% done".format(percent))
        plt.plot(x, U, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    elif j == int(T / 2):
        percent = int(50)
        print("{0:}% done".format(percent))
        plt.plot(x, U, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    elif j == int(3 * T / 4):
        percent = 75
        print("{0:}% done".format(percent))
        plt.plot(x, U, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    elif j == T - 1:
        print("100% done")
        plt.plot(x, U, label="$\\alpha = ${0:.4f}".format(maxT))

#plt.title('Diffusion of a catalytically reacting solute')

plt.xlabel('$z/l_f$', fontsize=12)
plt.ylabel('$C/C_o$', fontsize=12)
plt.legend(loc='upper right')
#plt.savefig('Hom_1D_CatalyticallyA.pdf')
plt.show()