import numpy as np
import matplotlib.pyplot as plt

# SETUP
N = 100   # number of x axis components
M = 100    # number of y axis components
maxT = float(input("What is the max value of alpha you want to got to?"))
maxW = 1
phi = float(input("What does the thiele modulus phi equal?"))
rho = float(input("What does rho equal?"))
a = phi**2
b = phi**2/rho
L = float(input("How long is the system?"))
H = float(input("How tall is the system?"))

T = int(np.ceil(maxT * ((a*maxW)/2 + 2*(N**2)*(H/L) + 2*(M**2)*(L/H))))
x = np.linspace(0., 1., N+1)
y = np.linspace(0., 1., M+1)
t = np.linspace(0., maxT, T+1)

dX = x[1] -x[0]
dY = y[1] -y[0]
dT = t[1] -t[0]

rX = (dT/(dX**2))* (H/L)
rY = (dT/(dY**2))* (L/H)

print("rX = {0}".format(rX))
print("rY = {0}".format(rY))
input("Press enter to continue.")

U = np.zeros((N+1,M+1))
U_new = np.zeros((N+1,M+1))
W = np.zeros((N+1,M+1))
W_new = np.zeros((N+1,M+1))

# INITIAL CONDITIONS
for j in range(0, M+1):
    U[-1,j] = U_new[-1,j] = 1  # outer area condition

# LOOP
a = 1
b = 1

for s in range(0, T):
    for j in range(1, M):
        for i in range(1, N):
            U_new[i, j] = (1-a*dT*W[i,j])*U[i, j] + \
                          rX * (U[i + 1, j] - 2*U[i, j] + U[i - 1, j]) +\
                          rY * (U[i, j + 1] - 2*U[i, j] + U[i, j - 1])
            W_new[i,j] = (1-b*dT*U[i,j])*W[i,j]
        U_new[1, j + 1] = U_new[0, j] = U_new[1, j]
    U = U_new
    W = W_new

    if ((s % 100) == 0):
        print("[0] {0} / {1}".format(s, T))
        str = "t = %d" % s
        fig = plt.figure()
        plt.plot([], [], ' ', label=str)
        plt.legend(loc='upper right', frameon=True)
        plt.title("2D Diffusion of a stiochiometrically reacting solute in a homogeneous biofilm")
        plt.imshow(U, vmin=0, vmax=1)
        plt.colorbar();
        plt.savefig("../Plots/Stiochiometrically/A/%d" % s, dpi=fig.dpi, bbox_inches='tight')
        plt.show()
    else:
        print("[0] {0}".format(s))