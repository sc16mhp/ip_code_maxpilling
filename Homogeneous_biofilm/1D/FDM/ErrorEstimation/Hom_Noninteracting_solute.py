import numpy as np
import matplotlib.pyplot as plt

# This code allows a user to input a max value of
# alpha and to get a plot of concentration for up
# to that value as diffusion goes into a
# non-interacting solute

N_values = [10, 20, 30, 40, 50]
maxT = float(input("What is the max value of alpha? (=D_e*t/epsilon_w*Lf^2)"))
count = 0
for N in N_values:
    # SETUP
    T = int(np.ceil(2*(1)*maxT * N**2))  # number of grid points in t axis

    x = np.linspace(0., 1., N+1)
    t = np.linspace(0., maxT, T+1)

    dX = x[1] - x[0]
    dT = t[1] - t[0]

    r = (dT / dX ** 2)
    print("r = {0}".format(r))

    U = np.zeros(N+1)
    U_new = np.zeros(N+1)

    Error = np.zeros(T+1)

    # INITIAL CONDITIONS
    for i in range(0, N+1):
        U[i] = 0

    # LOOP THROUGH TIME STEPS
    for j in range(0, T):
        for i in range(1, N):
            U_new[i] = r*U[i+1] + (1 - 2*r)*U[i] + r*U[i-1]
        U_new[0] = U_new[1]     # no flux constraint
        U_new[-1] = 1           # Boundary condition

        Error[j] = np.linalg.norm((U_new - U))
        U[:] = U_new
        if (j % 100) == 0:
            print("[{0}] {1} / {2}".format(count, j, T))
    plt.plot(t, np.log(Error), label="N={0}, M={1}, r={2:.2f}".format(N, T, r))
    count = count+1
#plt.title('Diffusion of a non-interacting solute (HOMOGENEOUS)')
x1, x2, y1, y2 = plt.axis()
# plt.axis((-0.01 ,0.5*maxT, y1, y2))
plt.xlabel('$\\alpha$', fontsize=12)
plt.ylabel('log(Error)', fontsize=12)
plt.legend(loc='upper right')
plt.savefig('ERROR_Hom_1D_NoninteractingBBBBB.pdf')
plt.show()