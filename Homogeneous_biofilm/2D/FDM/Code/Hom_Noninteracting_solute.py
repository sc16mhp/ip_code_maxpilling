import numpy as np
import matplotlib.pyplot as plt

#SETUP
N = 40   # number of x axis components
M = 40    # number of y axis components

maxT = float(input("What is the max value of alpha you want to got to?"))
L = float(input("How long is the system?"))
H = float(input("How tall is the system?"))

T = int(np.ceil(2*maxT* ((N**2)*(H/L) + (M**2)*(L/H))))
x = np.linspace(0., 1., N+1)
y = np.linspace(0., 1., M+1)
t = np.linspace(0., maxT, T+1)

dX = x[1] -x[0]
dY = y[1] -y[0]
dT = t[1] -t[0]

rX = (dT/(dX**2)) * (H/L)
rY = (dT/(dY**2)) * (L/H)

print("rX = {0}".format(rX))
print("rY = {0}".format(rY))
input("Press enter to continue.")

U = np.zeros((N+1,M+1))
U_new = np.zeros((N+1,M+1))

# INITIAL CONDITIONS
for j in range(0, M+1):
    U[-1,j] = U_new[-1,j] = 1  # outer area condition

# LOOP
for s in range(0, T):
    for j in range(1, M):
        for i in range(1, N):
            U_new[i, j] = U[i, j] + \
                          rX * (U[i + 1, j] - 2*U[i, j] + U[i - 1, j]) +\
                          rY * (U[i, j + 1] - 2*U[i, j] + U[i, j - 1])
        U_new[1, j + 1] = U_new[0, j] = U_new[1, j]
    U = U_new
    if ((s % 50) == 0):
        print("[0] {0} / {1}".format(s, T))
        print(U)
        str = "t = %d"%s
        fig = plt.figure()
        plt.plot([], [], ' ', label=str)
        plt.legend(loc='upper right', frameon=True)
        plt.title("2D Diffusion of a noninteracting solute in a homogeneous biofilm")
        plt.imshow(U,vmin=0, vmax=1)
        plt.imshow(U)
        plt.colorbar();
        plt.savefig("../Plots/Noninteracting/A/%d"%s, dpi=fig.dpi, bbox_inches='tight')
        plt.close()
        # plt.show()
    # else:
    #     print("[0] {0}".format(s))