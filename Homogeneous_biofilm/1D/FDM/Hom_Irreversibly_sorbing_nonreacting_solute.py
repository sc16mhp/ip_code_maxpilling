import numpy as np
import matplotlib.pyplot as plt

# This code allows a user to, once creating a function to describe the
# diffusivity of a irreversible sorbing nonreacting solute,
# and putting it in terms of non dimensional variables, AND finding
# the spatial derivative of said function. They can input the
# function into d(x,t) and d_dash(x,t) (derivative) and then they can
# run the code to generate results. The user inputs the MAX value
# of t at run time.
# Setting this too high, can render the project unstable.

# SETUP
N = 50     # number of grid points in the x axis

maxT = float(input("What is the max value of alpha you want to got to?"))
maxW = 1
phi = float(input("What does the thiele modulus phi equal?"))
rho = float(input("What does rho equal?"))
a = phi**2
b = phi**2/rho

T = int(np.ceil(maxT*(a*maxW/2 + 2*N**2)))  # number of grid points in t axis
x = np.linspace(0., 1., N+1)
t = np.linspace(0., maxT, T+1)

dX = x[1] - x[0]
dT = t[1] - t[0]

r = dT/(dX**2)

print("r = {0}".format(r))
input("Press enter to continue.")

U = np.zeros(N+1)
U_new = np.zeros(N+1)
W = np.zeros(N+1)
W_new = np.zeros(N+1)

# INITIAL CONDITIONS
for i in range(0, N+ 1):
    U[i] = 0
    W[i] = 1

# LOOP THROUGH TIME STEPS
for j in range(0, T):
    for i in range(1, N):
        U_new[i] = r*U[i+1] + (1-2*r - a*dT*W[i])*U[i] + r*U[i-1]
        W_new[i] = (1-b*dT*U[i])*W[i]

    U_new[0] = U_new[1]     # no flux constraint
    U_new[-1] = 1
    U[:] = U_new
    W[:] = W_new
    if j == int(T / 512):
        plt.plot(x, U, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    elif j == int(15 * T / 100):
        plt.plot(x, U, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    elif j == int(76 * T / 100):
        plt.plot(x, U, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    elif j == int(84 * T / 100):
        plt.plot(x, U, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    elif j == int(90 * T / 100):
        plt.plot(x, U, label="$\\alpha = ${0:.4f}".format(maxT * (j / T)))
    elif j == T - 1:
        print("100% done")
        plt.plot(x, U, label="$\\alpha = ${0:.4f}".format(maxT))

#plt.title('Diffusion of an irreversibly sorbing, nonreacting solute')

plt.xlabel('$z/l_f$', fontsize=12)
plt.ylabel('$C/C_o$', fontsize=12)
plt.legend(loc='upper right')
plt.savefig('Hom_1D_Irreversible.pdf')
plt.show()